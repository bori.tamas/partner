package com.mycomp.partner.mapper;

import com.mycomp.partner.domain.Partner;
import com.mycomp.partner.dto.PartnerDto;
import org.springframework.stereotype.Component;

@Component
public class PartnerDtoToPartnerMapper implements Mapper<PartnerDto, Partner> {
    
    @Override
    public Partner createDestination() {
        return new Partner();
    }

    @Override
    public Partner mapTo(PartnerDto partnerDto, Partner partner) {
        partner.setEmail(partnerDto.getEmail());
        partner.setName(partnerDto.getName());
        partner.setPhoneNumber(partnerDto.getPhoneNumber());

        return partner;
    }
    
}
