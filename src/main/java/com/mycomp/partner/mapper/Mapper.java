package com.mycomp.partner.mapper;

import java.util.*;

/**
 * Mapper between source and destination type.
 *
 * @param <SOURCE>
 *            the source type
 * @param <DESTINATION>
 *            the destination type
 */
public interface Mapper<SOURCE, DESTINATION> {

    /**
     * Create DESTINATION object.
     */
    DESTINATION createDestination();

    /**
     * Create DESTINATION object from the SOURCE object.
     *
     * @param source
     *          the source object
     * @return the converted object, or null, if the input was null.
     */
    default DESTINATION map(final SOURCE source) {
        if (source == null) {
            return null;
        }
        return mapTo(source, createDestination());
    }

    /**
     * Create {@see Optional<DESTINATION>} object from the {@code SOURCE} object.
     *
     * @param source
     *          the source object
     * @return the converted object, or empty {@link Optional}, if the input was null.
     */
    default Optional<DESTINATION> mapOptional(final SOURCE source) {
        return Optional.ofNullable(map(source));
    }

    /**
     * Create {@see Optional<DESTINATION>} object from the {@see Optional<SOURCE>} object.
     *
     * @param source the source object
     * @return the converted object, or empty {@link Optional}, if the input was empty.
     */
    default Optional<DESTINATION> mapOptional(final Optional<SOURCE> source) {
        if (source == null || !source.isPresent()) {
            return Optional.empty();
        }
        return source.map(this::map);
    }

    /**
     * Map DESTINATION object from the SOURCE object.
     *
     * @param source
     *            the source object
     * @param destination
     *            the destination object
     * @return the converted object, or null, if the input was null.
     */
    DESTINATION mapTo(SOURCE source, DESTINATION destination);

    /**
     * Map a collection of source objects to a list of destination objects. Uses the provided mapper method
     *
     * @param objects
     *            the collection of source objects
     * @return the list of mapped objects.
     */
    default List<DESTINATION> mapList(final Collection<SOURCE> objects) {
        if (objects == null || objects.isEmpty()) {
            return Collections.emptyList();
        }
        final List<DESTINATION> details = new ArrayList<>(objects.size());
        for (final SOURCE entity : objects) {
            details.add(map(entity));
        }
        return details;
    }

    /**
     * Map a collection of source objects to a set of destination objects.
     *
     * @param objects the collection of source objects
     * @return the set of mapped objects.
     */
    default Set<DESTINATION> mapSet(final Set<SOURCE> objects) {
        if (objects == null || objects.isEmpty()) {
            return Collections.emptySet();
        }
        final Set<DESTINATION> details = new HashSet<>(objects.size() * 4 / 3 + 1);
        for (final SOURCE entity : objects) {
            details.add(map(entity));
        }
        return details;
    }
}
