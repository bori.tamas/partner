package com.mycomp.partner.mapper;

import com.mycomp.partner.domain.Partner;
import com.mycomp.partner.dto.PartnerDto;
import org.springframework.stereotype.Component;

@Component
public class PartnerToPartnerDtoMapper implements Mapper<Partner, PartnerDto> {
    
    @Override
    public PartnerDto createDestination() {
        return new PartnerDto();
    }

    @Override
    public PartnerDto mapTo(Partner partner, PartnerDto partnerDto) {
        partnerDto.setEmail(partner.getEmail());
        partnerDto.setName(partner.getName());
        partnerDto.setPhoneNumber(partner.getPhoneNumber());

        return partnerDto;
    }
}
