package com.mycomp.partner.exception;

public class RecordNotFoundException extends RuntimeException {

    public static final Long serialVersionUID = 34577686L;

    /**
     * Exception to be thrown when the given record is not found in DB.
     */
    public RecordNotFoundException(String message) {
        super(message);
    }

}
