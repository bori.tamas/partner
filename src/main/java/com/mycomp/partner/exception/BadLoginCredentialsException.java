package com.mycomp.partner.exception;

/**
 * Represents the Exception object when there is no user with the specified name.
 */
public class BadLoginCredentialsException extends RuntimeException {

    private static final long serialVersionUID = 4469232892888683720L;

    private static final String exceptionName = "BadLoginCredentials";

    public static String getExceptionName() {
        return exceptionName;
    }

    /**
     * Exception to be thrown during an User query for a specific email address.
     */
    public BadLoginCredentialsException(String name) {
        super("Bad credentials! Wrong user name and/or password combination for " + name + "!");
    }
}
