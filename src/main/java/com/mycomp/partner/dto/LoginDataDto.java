package com.mycomp.partner.dto;

import javax.validation.constraints.NotBlank;

/**
 * The object holds the login data, that the clients sends during the login attempt.
 */
public class LoginDataDto {

    @NotBlank(message = "User name can't be empty!")
    private String userName;

    @NotBlank(message = "Password can't be blank!")
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}

