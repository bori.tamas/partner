package com.mycomp.partner.service;

import com.mycomp.partner.domain.Partner;
import com.mycomp.partner.exception.RecordNotFoundException;
import com.mycomp.partner.repository.PartnerRepository;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PartnerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartnerService.class);
    
    @Autowired
    private PartnerRepository partnerRepository;

    public List<Partner> getPartners() {
        return partnerRepository.findAll();
    }

    public Partner getPartnerById(UUID partnerId) {
        return partnerRepository.findById(partnerId).
                orElseThrow(() -> new RecordNotFoundException("Could not found partner with ID: " + partnerId));
    }

    public UUID createPartner(Partner partner) {
//        partner.setId(UUID.randomUUID());
        Partner savedPartner = partnerRepository.save(partner);
        return savedPartner.getId();
    }
    
    public void updatePartner(Partner partner) {
        partnerRepository.save(partner);
    }
    
    public boolean deletePartner(Partner partner) {
        if (partnerRepository.existsById(partner.getId())) {
            partnerRepository.delete(partner);
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Method for filtering and sorting of partner entities.
     */
    @Transactional
    public String exportPartnersToCsv() {
        LOGGER.info("Export Partner entities in csv format"
                + ", with parameters: {} {} {}");

        List<Partner> partners = partnerRepository.findAll();

        StringBuilder csvString = new StringBuilder();
        partners.forEach((h) -> {
            csvString.append(h.toCsv());
        });

        LOGGER.info("The content of the csv file: {}", csvString);

        return csvString.toString();
    }
}
