package com.mycomp.partner.service;

import com.mycomp.partner.domain.PartnerAddress;
import com.mycomp.partner.exception.RecordNotFoundException;
import com.mycomp.partner.repository.PartnerAddressRepository;
import java.util.List;
import java.util.UUID;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PartnerAddressService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartnerAddressService.class);

    @Autowired
    private PartnerAddressRepository partnerAddressRepository;

    public List<PartnerAddress> getPartnerAddresses() {
        return partnerAddressRepository.findAll();
    }

    public PartnerAddress getPartnerAddressById(UUID addressId) {
        return partnerAddressRepository.findById(addressId).
                orElseThrow(() -> new RecordNotFoundException("Could not found address with ID: " + addressId));
    }

    public UUID createPartner(PartnerAddress partnerAddress) {
        PartnerAddress savedPartnerAddress = partnerAddressRepository.save(partnerAddress);
        return savedPartnerAddress.getPartnerId();
    }

    public void updatePartner(PartnerAddress partner) {
        partnerAddressRepository.save(partner);
    }

    public boolean deletePartner(PartnerAddress partner) {
        if (partnerAddressRepository.existsById(partner.getPartnerId())) {
            partnerAddressRepository.delete(partner);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method for filtering and sorting of partnerAddress entities.
     */
    @Transactional
    public String exportPartnerAddressToCsv() {
        LOGGER.info("Export PartnerAddress entities in csv format"
                + ", with parameters: {} {} {}");

        List<PartnerAddress> partners = partnerAddressRepository.findAll();

        StringBuilder csvString = new StringBuilder();
        partners.forEach((h) -> {
            csvString.append(h.toCsv());
        });

        LOGGER.info("The content of the csv file: {}", csvString);

        return csvString.toString();
    }
}
