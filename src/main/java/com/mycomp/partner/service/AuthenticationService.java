package com.mycomp.partner.service;

import com.mycomp.partner.dto.LoginDataDto;
import com.mycomp.partner.dto.LoginResponseDto;

/**
 * The SecurityService for security/Authentication related functions.
 */
public interface AuthenticationService {

    /**
     * The service for handling the login.
     *
     * @param loginDataDto - The LoginDto object, that is received from the client.
     * @return - The response object with the user parameters and the JWT token.
     */
    LoginResponseDto login(LoginDataDto loginDataDto);

}
