package com.mycomp.partner.service;

import com.mycomp.partner.domain.User;
import com.mycomp.partner.dto.LoginDataDto;
import com.mycomp.partner.dto.LoginResponseDto;
import com.mycomp.partner.exception.BadLoginCredentialsException;
import com.mycomp.partner.repository.UserRepository;
import com.mycomp.partner.security.token.JwtTokenProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.security.crypto.password.PasswordEncoder;

@Service
public class PartnerAuthenticationService implements AuthenticationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartnerAuthenticationService.class);

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public LoginResponseDto login(LoginDataDto loginDataDto) {

        LOGGER.info("Logging in user with user name: {}",
                loginDataDto.getUserName());

        String name = loginDataDto.getUserName().toLowerCase();

        User user = userRepository.findByName(name)
                .orElseThrow(() -> new BadLoginCredentialsException(name));

        if (!passwordEncoder.matches(loginDataDto.getPassword(), user.getPassword())) {
            throw new BadLoginCredentialsException(name);
        }

        String token = jwtTokenProvider
                .createToken(name);

        LoginResponseDto dto = new LoginResponseDto();
        dto.setName(name);
        dto.setToken(token);
        
        return dto;
    }
}
