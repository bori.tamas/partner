package com.mycomp.partner.controller;

import com.mycomp.partner.domain.Partner;
import com.mycomp.partner.dto.PartnerDto;
import com.mycomp.partner.mapper.PartnerDtoToPartnerMapper;
import com.mycomp.partner.service.PartnerService;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/partner")
public class PartnerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartnerController.class);
    
    @Autowired
    private PartnerService partnerService;
    
    @Autowired
    private PartnerDtoToPartnerMapper partnerDtoToPartnerMapper;
    
    @Value("${partner.csv.filename}")
    private String csvFileName;
    
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Partner> listPartners() {
        return partnerService.getPartners();
    }
    
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Partner getPartnerByAddress(@PathVariable UUID id) {
        return partnerService.getPartnerById(id);
    }
    
    /**
     * API endpoint for creating a new partner.
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UUID createPartner(@Valid @RequestBody PartnerDto partnerDto) {
        return partnerService.createPartner(partnerDtoToPartnerMapper.map(partnerDto));
    }
    
    /**
     * API endpoint for updating a partner.
     */
    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void updateUser(@Valid @RequestBody PartnerDto partnerDto) {
       partnerService.updatePartner(partnerDtoToPartnerMapper.map(partnerDto));
    }
    
    /**
     * API endpoint for updating the roles of a user.
     *
     * @param userWithRoleDto UserWithRoleDto that holds roles of a user.
     * 
     * @return the id of the user
     */
    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void deleteUser(@Valid @RequestBody Partner partner) {
       partnerService.deletePartner(partner);
    }
    
    /**
     * API endpoint for search and get Harbour entities in csv format.
     */
    @GetMapping(value = "/list/csv",
            produces = {MediaType.APPLICATION_JSON_VALUE,
                    "text/csv;charset=UTF-8;Content-Disposition;attachment"})
    @ResponseStatus(HttpStatus.OK)
    public byte[] exportPartnersToCsv(HttpServletResponse response) {
        LOGGER.info("Request for exporting Partner entities to Csv");

        response.addHeader("filename", csvFileName);
        return partnerService.exportPartnersToCsv().getBytes();
    }
}
