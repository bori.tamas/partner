package com.mycomp.partner.controller;

import com.mycomp.partner.dto.LoginDataDto;
import com.mycomp.partner.dto.LoginResponseDto;
import com.mycomp.partner.service.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * The RestController for the authentication endpoints.
 */
@RestController
@RequestMapping("/auth")
public class AuthController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

    @Value("${spring.application.server.url}")
    private String serverUrl;

    @Autowired
    private AuthenticationService security;

    @Autowired
    HttpSession session;


    /**
     * The endpoint for login.
     *
     * @param loginDataDto - The Login credentials from the client.
     * @return - A LoginResponseDto with the necessary information.
     */
    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public LoginResponseDto login(@RequestBody @Valid LoginDataDto loginDataDto) {
        LOGGER.info(String.format("Login attempt on /auth/login with user code: %1$s", loginDataDto.getUserName()));

        return security.login(loginDataDto);
    }

}
