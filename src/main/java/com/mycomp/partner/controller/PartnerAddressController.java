/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycomp.partner.controller;

import com.mycomp.partner.domain.PartnerAddress;
import com.mycomp.partner.service.PartnerAddressService;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/address")
public class PartnerAddressController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartnerAddressController.class);

    @Autowired
    private PartnerAddressService partnerAddressService;

    @Value("${address.csv.filename}")
    private String csvFileName;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<PartnerAddress> listPartners() {
        return partnerAddressService.getPartnerAddresses();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PartnerAddress getPartnerByAddress(@PathVariable UUID id) {
        return partnerAddressService.getPartnerAddressById(id);
    }

    /**
     * API endpoint for creating a new partner.
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UUID createPartner(@Valid @RequestBody PartnerAddress partnerAddress) {
        return partnerAddressService.createPartner(partnerAddress);
    }

    /**
     * API endpoint for updating a partner.
     */
    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void updateUser(@Valid @RequestBody PartnerAddress partnerAddress) {
        partnerAddressService.updatePartner(partnerAddress);
    }

    /**
     * API endpoint for updating the roles of a user.
     *
     * @param userWithRoleDto UserWithRoleDto that holds roles of a user.
     *
     * @return the id of the user
     */
    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void deleteUser(@Valid @RequestBody PartnerAddress partnerAddress) {
        partnerAddressService.deletePartner(partnerAddress);
    }

    /**
     * API endpoint for search and get Harbour entities in csv format.
     */
    @GetMapping(value = "/list/csv",
            produces = {MediaType.APPLICATION_JSON_VALUE,
                "text/csv;charset=UTF-8;Content-Disposition;attachment"})
    @ResponseStatus(HttpStatus.OK)
    public byte[] exportPartnersToCsv(HttpServletResponse response) {
        LOGGER.info("Request for exporting PartnerAddress entities to Csv");

        response.addHeader("filename", csvFileName);
        return partnerAddressService.exportPartnerAddressToCsv().getBytes();
    }
}
