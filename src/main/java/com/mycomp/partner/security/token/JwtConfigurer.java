package com.mycomp.partner.security.token;

import com.mycomp.partner.security.PartnerAuthenticationEntryPoint;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;

public class JwtConfigurer extends
        SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    private JwtTokenProvider jwtTokenProvider;

    private JwtExceptionFilter jwtExceptionFilter;

    /**
     * The configurer constructor, for injecting the needed objects.
     *
     * @param jwtTokenProvider   - JwtTokenProvider object.
     * @param jwtExceptionFilter - JwtExceptionFilter object.
     */
    public JwtConfigurer(JwtTokenProvider jwtTokenProvider,
                         JwtExceptionFilter jwtExceptionFilter) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.jwtExceptionFilter = jwtExceptionFilter;
    }

    /**
     * Method for configuring the JWT Token.
     *
     * @param http - HttpSecurity object.
     * @throws Exception - The exception that has been thrown during configuration.
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {

        JwtTokenFilter tokenFilter = new JwtTokenFilter(jwtTokenProvider);
        http.exceptionHandling()
                .authenticationEntryPoint(new PartnerAuthenticationEntryPoint())
                .and().addFilterBefore(tokenFilter, UsernamePasswordAuthenticationFilter.class)
                .addFilterAfter(jwtExceptionFilter, CorsFilter.class);
    }
}

