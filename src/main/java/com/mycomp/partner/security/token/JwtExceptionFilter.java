package com.mycomp.partner.security.token;

import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtExceptionFilter extends OncePerRequestFilter {

    @Autowired
    private HandlerExceptionResolver handlerExceptionResolver;

    /**
     * Method for filtering the requests.
     *
     * @param httpServletRequest  - Request object.
     * @param httpServletResponse - Response object.
     * @param filterChain         - FilterChain object.
     * @throws ServletException - The ServletException object that is thrown during internal
     *                          filter.
     * @throws IOException      - The IOException that is thrown during internal filer.
     */
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse, FilterChain filterChain)
            throws ServletException, IOException {

        try {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } catch (JwtException ex) {
            handlerExceptionResolver.resolveException(httpServletRequest, httpServletResponse, null,
                    new Exception(ex.getMessage()));
        }
    }
}