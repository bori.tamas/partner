package com.mycomp.partner.security.token;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

@Component
public class JwtTokenProvider {

    @Value("${security.jwt.token.secret-key}")
    private String secretKey;

    @Value("${security.jwt.token.expire-length}")
    private long timeLimit;
    
    @Autowired
    private UserDetailsService userDetailsService;

    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    /**
     * Creates token based on username and user's role.
     *
     * @param userCode username
     * @param roles    list of roles
     * @return JWT token
     */
    public String createToken(String userName) {

        Claims claims = Jwts.claims().setSubject(userName);

        Date now = Calendar.getInstance().getTime();
        Date validity = new Date(now.getTime() + timeLimit);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    /**
     * Getting username based on the given token.
     *
     * @param token token
     * @return username
     */
    public String getUsername(String token) {

        return Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
    }

    /**
     * Resolving given token.
     *
     * @param servletRequest servlet request
     * @return token without "Bearer " prefix
     */
    public String resolveToken(HttpServletRequest servletRequest) {

        String bearerToken = servletRequest.getHeader("Authorization");

        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }

        return null;
    }

    /**
     * The method validates the token for expiration.
     *
     * @param token - The token for validation.
     * @return - Returns the token validity.
     */
    public boolean validateToken(String token) {

        Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);

        return Calendar.getInstance().getTime().before(claims.getBody().getExpiration());
    }
    
    /**
     * Gives authentication based on the given token.
     *
     * @param token token
     * @return token with user's authorities
     */
    public Authentication getAuthentication(String token) {
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(getUsername(token));

        return new UsernamePasswordAuthenticationToken(userDetails, "",
                userDetails.getAuthorities());
    }

}

