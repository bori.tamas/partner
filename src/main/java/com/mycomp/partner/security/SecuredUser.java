package com.mycomp.partner.security;

import com.mycomp.partner.domain.User;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

public class SecuredUser implements UserDetails {

    private static final long serialVersionUID = 5184679875322009689L;

    private User user;

    private Set<SimpleGrantedAuthority> grantedAuthorities = new HashSet<>();

    /**
     * The constructor for defining a User entity.
     *
     * @param user - A user entity.
     */
    public SecuredUser(User user) {
        this.user = user;
    }

    /**
     * The method for returning the collection of SimpleGrantedAuthority objects.
     *
     * @return the set holding the SimpleGrantedAuthority objects with the ElementaryRight codes.
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    /**
     * The method for setting the collection of SimpleGrantedAuthority objects.
     */
    public void setGrantedAuthorities(Set<SimpleGrantedAuthority> grantedAuthorities) {
        this.grantedAuthorities = grantedAuthorities;
    }

    /**
     * Method, for returning the password.
     *
     * @return - The password.
     */
    @Override
    public String getPassword() {
        return user.getPassword();
    }

    /**
     * Method, for returning the username/email address.
     *
     * @return - The username/email address.
     */
    @Override
    public String getUsername() {
        return user.getName();
    }

    public UUID getUserId() {
        return user.getId();
    }

    public User getUser() {
        return user;
    }

    @Override
    public boolean isAccountNonExpired() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isAccountNonLocked() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isCredentialsNonExpired() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isEnabled() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
