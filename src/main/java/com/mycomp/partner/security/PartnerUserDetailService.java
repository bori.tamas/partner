package com.mycomp.partner.security;

import com.mycomp.partner.domain.User;
import com.mycomp.partner.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class PartnerUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    /**
     * The method for loading the user as a SecuredUser.
     *
     * @param userCode - The email address.
     * @return - UserDetails object.
     * @throws UsernameNotFoundException - The exception that is thrown when something goes wrong during user load from
     *                                   the DB.
     */
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String userName) {
        User user = userRepository.findByName(userName).get();

        SecuredUser securedUser = new SecuredUser(user);
        return securedUser;
    }

}