package com.mycomp.partner.security;

import com.mycomp.partner.security.token.JwtConfigurer;
import com.mycomp.partner.security.token.JwtTokenProvider;
import com.mycomp.partner.security.token.JwtExceptionFilter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private PartnerUserDetailService userDetailsService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private JwtExceptionFilter jwtExceptionFilter;

    @Value("#{'${cors.allowed-origins}'.split(',')}")
    private List<String> corsAllowedOrigins;

    @Value("#{'${cors.allowed-methods}'.split(',')}")
    private List<String> corsAllowedMethods;

    @Value("#{'${cors.allowed-headers}'.split(',')}")
    private List<String> corsAllowedHeaders;

    private static final String[] AUTH_WHITELIST = {
            "/health/**",
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/webjars/**"
    };

    private static final String[] POST_WHITELIST = {
            "/auth/login"
    };


    /**
     * Method for configuring the UserDetail service and the encoder for the authentication.
     *
     * @param auth - The AuthenticationManagerBuilder that needs to be configured.
     * @throws Exception - The exception that is thrown during configuration.
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(AUTH_WHITELIST).permitAll()
                .antMatchers(HttpMethod.POST, POST_WHITELIST).permitAll()
                .anyRequest().authenticated().and()
                .cors().and().apply(new JwtConfigurer(jwtTokenProvider, jwtExceptionFilter));
    }

    /**
     * Defining the authenticationManager.
     *
     * @return - The AuthenticationManagerBean.
     * @throws Exception - The exception that is thrown during bean initialization.
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * Setting passwordEncoder to a BCryptEncoder.
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();

        corsConfiguration.setAllowedOrigins(corsAllowedOrigins);
        corsConfiguration.setAllowedMethods(corsAllowedMethods);
        corsConfiguration.setAllowCredentials(true);
        corsConfiguration.setAllowedHeaders(corsAllowedHeaders);
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource
                = new UrlBasedCorsConfigurationSource();
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);

        return urlBasedCorsConfigurationSource;
    }
}
