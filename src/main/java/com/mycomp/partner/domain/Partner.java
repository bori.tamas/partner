/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycomp.partner.domain;

import java.util.StringJoiner;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;

/**
 *
 * @author borit
 */
@Entity
@Audited
@Table(name = "partner")
@DynamicUpdate
@DynamicInsert
public class Partner {
     @Id
    @Column(name = "id", nullable = false)
    private UUID id;

    @Email(message = "Given email format is not correct.")
    @Column(name = "e_mail")
    private String email;

    @Column(name = "name")
    private String  name;

    @Column(name = "phone_number")
    private String phoneNumber;

    /**
     * @return the id
     */
    public UUID getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(UUID id) {
        this.id = id;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    /**
     * Returns with the csv format string of the entity.
     */
    public String toCsv() {
        StringJoiner csvFormat = new StringJoiner(";");

        csvFormat.add(this.getId().toString())
                .add(this.getName())
                .add(this.getEmail().toString())
                .add(this.getPhoneNumber().toString())
                .add(System.getProperty("line.separator"));

        return csvFormat.toString();
    }
}
