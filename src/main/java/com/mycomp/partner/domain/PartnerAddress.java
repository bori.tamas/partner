package com.mycomp.partner.domain;

import java.util.StringJoiner;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "partner_address")
@DynamicUpdate
@DynamicInsert
public class PartnerAddress {
    
    @Id
    @Column(name = "address_id", nullable = false)
    private UUID addressId;
    
    @Column(name = "partner_id", nullable = false)
    private UUID partnerId;

    @Column(name = "zipcode")
    private String  zipCode;

    @Column(name = "country")
    private String country;
    
    @Column(name = "street")
    private String street;

    /**
     * @return the addressId
     */
    public UUID getAddressId() {
        return addressId;
    }

    /**
     * @param addressId the addressId to set
     */
    public void setAddressId(UUID addressId) {
        this.addressId = addressId;
    }

    /**
     * @return the partnerId
     */
    public UUID getPartnerId() {
        return partnerId;
    }

    /**
     * @param partnerId the partnerId to set
     */
    public void setPartnerId(UUID partnerId) {
        this.partnerId = partnerId;
    }

    /**
     * @return the zipCode
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * @param zipCode the zipCode to set
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }
    
    /**
     * Returns with the csv format string of the entity.
     */
    public String toCsv() {
        StringJoiner csvFormat = new StringJoiner(";");

        csvFormat.add(this.getAddressId().toString())
                .add(this.getCountry())
                .add(this.getZipCode())
                .add(this.getStreet())
                .add(System.getProperty("line.separator"));

        return csvFormat.toString();
    }
}
