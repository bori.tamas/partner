/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycomp.partner.repository;

import com.mycomp.partner.domain.Partner;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PartnerRepository extends JpaRepository<Partner, UUID> {
    
    List<Partner> findAll();
    
    /**
     * Checks if a user with the given email already exists in DB.
     */
    boolean existsByName(String name);

    /**
     * Finds the user entity in the DB by the given username.
     */
    Optional<Partner> findByName(String name);
}
