/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycomp.partner.repository;

import com.mycomp.partner.domain.PartnerAddress;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PartnerAddressRepository extends JpaRepository<PartnerAddress, UUID> {
        
    List<PartnerAddress> findAll();
    
}
