package com.mycomp.partner.repository;

import com.mycomp.partner.domain.User;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
    
    /**
     * Checks if a user with the given email already exists in DB.
     */
    boolean existsByName(String name);

    /**
     * Finds the user entity in the DB by the given username.
     */
    Optional<User> findByName(String name);
}
